<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function create(){
        return view('data.index_create');
    }

    public function store(Request $request) {
        $request->validate([
                    'nama' => 'required|unique:cast',
                    'umur' => 'required',
                    'bio' => 'required'
                ]);

        $cast = new Cast;
        $cast->nama = $request["nama"];
        $cast->umur = $request["umur"];
        $cast->bio = $request["bio"];
        $cast->save();

        return redirect('/cast')->with('success', 'Cast berhasil disimpan!');
    }
    // public function store(Request $request){
    //     // dd($request->all());
    //     $request->validate([
    //         'nama' => 'required|unique:cast',
    //         'umur' => 'required',
    //         'bio' => 'required'
    //     ]);
    //     $query = DB::table('cast')->insert([
    //         'nama' => $request["nama"],
    //         'umur' => $request["umur"],
    //         'bio' => $request["bio"]
    //     ]);
    //     return redirect('/cast')->with('success', 'Cast berhasil disimpan!');
    // }

    public function index(){
        // $cast = DB::table('cast')->get();
        // dd($cast);
        $cast = Cast::all();

        return view('data.index', compact('cast'));
    }

    public function detail($cast_id){
        // $cast = DB::table('cast')->where('id', $cast_id)->first();
        // dd($cast);
        $cast = Cast::find($cast_id);

        return view('data.cast_detail', compact('cast'));
    }

    public function edit($cast_id){
        // $cast = DB::table('cast')->where('id', $cast_id)->first();
        $cast = Cast::find($cast_id);

        return view('data.cast_edit', compact('cast'));
    }

    public function update($cast_id, Request $request){
        // $query = DB::table('cast')
        //                         ->where('id', $cast_id)
        //                         ->update([
        //                             'nama' => $request['nama'],
        //                             'umur' => $request['umur'],
        //                             'bio'  => $request['bio']
        //                         ]);
        $cast = Cast::where('id', $cast_id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);
        
        return redirect('/cast')->with('success', 'Berhasil update data!');
    }

    public function destroy($cast_id){
        // $query = DB::table('cast')->where('id',$cast_id)->delete();
        $cast = Cast::destroy($cast_id);
        return redirect('/cast')->with('success', 'Berhasil delete data!');
    }
}
