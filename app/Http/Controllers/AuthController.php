<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function sign_up_POST(Request $request){
        $nama_depan = $request["fname"];
        $nama_belakang = $request["lname"];
        return view('welcome', compact('nama_depan'), compact('nama_belakang'));
    }
}
