@extends('layout.master')

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Cast {{$cast->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{route('casting.update', ['casting' => $cast->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama) }}" placeholder="Enter name">
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message}}</div>
                    @enderror
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', $cast->umur) }}"placeholder="age">
                    </div>
                    @error('umur')
                        <div class="alert alert-danger">{{ $message}}</div>
                    @enderror
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $cast->bio) }}"placeholder="bio">
                    </div>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message}}</div>
                    @enderror
                </div>
                <!-- /.card-body -->
        
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">edit data</button>
                </div>
            </form>
        </div>
    </div>
    @endsection