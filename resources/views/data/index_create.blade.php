@extends('layout.master')

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">New Cast</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{route('casting.store')}}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Enter name">
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message}}</div>
                    @enderror
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur'. '') }}"placeholder="age">
                    </div>
                    @error('umur')
                        <div class="alert alert-danger">{{ $message}}</div>
                    @enderror
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}"placeholder="bio">
                    </div>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message}}</div>
                    @enderror
                </div>
                <!-- /.card-body -->
        
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">tambah data</button>
                </div>
            </form>
        </div>
    </div>
@endsection