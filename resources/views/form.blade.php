<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Sign Up Form</h1>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First Name:</label>
        <br>
        <input type="text" id="fname" name="fname"><br>
        <br>
        <label for="lname">Last Name:</label>
        <br>
        <input type="text" id="lname" name="lname"><br>
        <br>

        <label for="">Gender:</label>
        <br>
        <input type="radio" name="Gender" value="Male">
        <label for="Male">Male</label>
        <br>
        <input type="radio" name="Gender" value="female">
        <label for="female">Female</label>
        <br>
        <input type="radio" name="Gender" value="other">
        <label for="other">Other</label><br>
        <br>

        <label for="warga-negara">Nationality:</label><br><br>
        <select name="warga-negara" id="negara">
            <option value="WNI">Indonesia</option>
            <option value="WNA">lainnya</option>
        </select>
        <br><br>

        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" name="Language" value="indonesia">
        <label for="Male">Indonesia</label>
        <br>
        <input type="checkbox" name="Language" value="english">
        <label for="female">English</label>
        <br>
        <input type="checkbox" name="Language" value="other">
        <label for="other">Other</label><br>
        <br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="Bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <button>Sign up</button>
    </form>
</body>
</html>