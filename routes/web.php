<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/form', 'AuthController@form');

Route::post('/welcome', 'AuthController@sign_up_POST');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/data-tables', function(){
    return view('items.data-tables');
});

Route::get('/table', function(){
    return view('items.table');
});

Route::get('/cast', 'CastController@index');

Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');

Route::get('/cast/{cast_id}', 'CastController@detail');

Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::put('/cast/{cast_id}', 'CastController@update');

Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::resource('casting', 'CastingController');