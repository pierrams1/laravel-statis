<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmsIdToPeran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peran', function (Blueprint $table) {
            $table->unsignedBigInteger('films_id');
            $table->foreign('films_id')->references('id')->on('films');
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peran', function (Blueprint $table) {
            $table->dropForeign(['films_id']);
            $table->dropColumn(['films_id']);
            $table->dropForeign(['cast_id']);
            $table->dropColumn(['cast_id']);
        });
    }
}
